FLATPAK=$(shell which flatpak)
FLATPAK_BUILDER=$(shell which flatpak-builder)
PYTHON=$(shell which python3)

FLATPAK_MANIFEST=go-astielectron-flatpak-offline-demo.yml
FLATPAK_APPID=com.gitlab.go-astilectron-offline-flatpak-demo

FLATPAK_BUILD_FLAGS := --verbose --force-clean --install-deps-from=flathub
FLATPAK_INSTALL_FLAGS := --verbose --user --install --force-clean
FLATPAK_DEBUG_FLAGS := --verbose --run
FLATPAK_RUN_FLAGS := --arch=x86_64

.PHONY: build-dependencies
build-dependencies:
	$(FLATPAK) install org.freedesktop.Sdk.Extension.golang//21.08
	$(FLATPAK) install org.freedesktop.Platform//21.08
	$(FLATPAK) install org.freedesktop.Sdk//21.08
	$(FLATPAK) install org.electronjs.Electron2.BaseApp//21.08

.PHONY: build
build:
	@echo "Building flatpak..."
	$(FLATPAK_BUILDER) build $(FLATPAK_BUILD_FLAGS) $(FLATPAK_MANIFEST)

.PHONY: install
install:
	@echo "Installing flatpak..."
	$(FLATPAK_BUILDER) build $(FLATPAK_INSTALL_FLAGS) $(FLATPAK_MANIFEST)

.PHONY: uninstall
uninstall:
	$(FLATPAK) uninstall --delete-data --assumeyes $(FLATPAK_APPID)

.PHONY: debug
debug:
	$(FLATPAK_BUILDER) $(FLATPAK_DEBUG_FLAGS) build $(FLATPAK_MANIFEST) sh

.PHONY: debug-installed
debug-installed:
	$(FLATPAK) run --command=sh --devel $(FLATPAK_APPID)

.PHONY: run
run:
	$(FLATPAK) run $(FLATPAK_RUN_FLAGS) $(FLATPAK_APPID)

.PHONY: generate-astilectron-bundler-sources
generate-astilectron-bundler-sources:
	$(FLATPAK_BUILDER) build astilectron-bundler-download-manifest.yml \
		--verbose \
		--keep-build-dirs \
		--force-clean
	$(PYTHON) flatpak-builder-tools/go-get/flatpak-go-get-generator.py .flatpak-builder/build/astilectron-bundler

.PHONY: generate-go-astilectron-demo-sources
generate-go-astilectron-demo-sources:
	$(FLATPAK_BUILDER) build go-astilectron-demo-download-manifest.yml \
		--verbose \
		--keep-build-dirs \
		--force-clean
	$(PYTHON) flatpak-builder-tools/go-get/flatpak-go-get-generator.py .flatpak-builder/build/go-astilectron-demo
