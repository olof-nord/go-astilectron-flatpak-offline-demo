# go-astilectron-flatpak-offline-demo

Demo go-astilectron-demo app with offline Flatpak packaging, suitable for Flathub publishing.

https://github.com/asticode/go-astilectron-demo

## Build

To build the Flatpak, use the following:

`make build`

## Install

To install the Flatpak, use the following:

`make install`

## Debug

To debug (open a shell to it) the built Flatpak, use the following:

`make debug`

## Run

To run the Flatpak, use the following:

`make run`


## astilectron offline bundler

Instead of downloading Electron and Astilectron during build, the archives are downloaded with the Flatpak manifest.

This is done by specifying `working_directory_path` in the bundler.json file, and downloading the archives to a `cache/`
directory in beforehand. Note that the archives should not be extracted: that is done by the bundler.

Many thanks to [@asticode](https://github.com/asticode) for clarifying this
in [issue 101](https://github.com/asticode/go-astilectron-bundler/issues/101)!

## cross-compile

It is possible to specify for which arch a build should be compiled if the necessary binfmt dependencies are installed.

```shell
make build FLATPAK_BUILD_FLAGS="--verbose --force-clean --install-deps-from=flathub --arch=aarch64"
```
```shell
make install FLATPAK_INSTALL_FLAGS="--verbose --user --install --force-clean --arch=aarch64"
```

```shell
make run FLATPAK_RUN_FLAGS="--arch=aarch64"
```
